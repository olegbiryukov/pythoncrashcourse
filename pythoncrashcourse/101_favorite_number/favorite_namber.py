import json

def read_number():
    try:
        with open(filename) as f:
            number = json.load(f)
    except FileNotFoundError:
        return None
    else:
        return number

def remember_number():
    number = input(f"Enter your favorite number: ")
    with open(filename, 'w') as f:
        json.dump(number, f)
    return number

def tell_number():
    number = read_number()
    if number:
         print(f"Your favorite namber is {number}!")
    else:
        number = remember_number()
        print(f"Your favorite number \"{number}\" was remembered!")

filename = '/home/oleg/code/lessons/pythoncrashcourse/101_favorite_number/number.json'

tell_number()