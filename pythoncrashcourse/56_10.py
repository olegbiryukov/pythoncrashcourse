number = input("Enter a number, and I'll tell you if it is a multiple of ten: ")
number = int(number)
if number % 10 == 0:
    print(f"\nThe number {number} is multiple of ten.")
else:
    print(f"\nThe number {number} isn't multiple of ten.")