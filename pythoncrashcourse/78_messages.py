def show_messages(messages):
    for message in messages:
        message = f"{message.title()} - this is a message!"
        print(message)

def send_messages(messages, sent_messages):
    while messages:
        current_message = messages.pop()
        sent_messages.append(current_message)

messages = ['hello', 'hi', 'privet', 'zdravstvuite']
sent_messages = []

show_messages(messages)
send_messages(messages[:], sent_messages)

print(messages)
print(sent_messages)