cars = ["bmw", "audi", "subaru", "toyota"]
cars.sort()
print(cars)

cars.sort(reverse=True)
print(cars)

print("\nHere is the original list:")
print(cars)
print("\nHere is the sorted list:")
print(sorted(cars))
print("\nHere is the original list again:")
print(cars)
print("\nHere is reversed list:")
cars.reverse()
print(cars)
print(len(cars))