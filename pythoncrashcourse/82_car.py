def make_info(name, model, **info):
    info['car_name'] = name
    info['car_model'] = model
    return info

car_info = make_info('subaru', 'outback', color='blue', tyres='16-inch')

print(car_info)