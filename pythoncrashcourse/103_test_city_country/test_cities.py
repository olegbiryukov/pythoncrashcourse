import unittest
from city_functions import get_location_names

class LocationNameTest(unittest.TestCase):

    def test_city_country(self):
        location = get_location_names('chilie', 'santiago')
        self.assertEqual(location, 'Santiago, Chilie')

    def test_city_country_population(self):
        location = get_location_names('chilie', 'santiago', 5000000)
        self.assertEqual(location, 'Santiago, Chilie - population 5000000')

if __name__ == '__main__':
    unittest.main()