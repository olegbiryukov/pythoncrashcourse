def get_location_names(country, city, population=''):
    if population:
        location = f"{city.title()}, {country.title()} - population {population}"
    else:
        location = f"{city.title()}, {country.title()}"
    return location
