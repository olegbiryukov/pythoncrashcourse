class User():
    def __init__(self, first_name, last_name, age, gender, login_attempts):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.login_attempts = login_attempts
    
    def describe_user(self):
        if self.gender == 'male':
            print(f"{self.first_name.title()} {self.last_name.title()} is a {self.gender} and he is {self.age} years old.")
        elif self.gender == 'female':
            print(f"{self.first_name.title()} {self.last_name.title()} is a {self.gender} and she is {self.age} years old.")
        else:
            print(f"{self.first_name.title()} {self.last_name.title()} is a {self.gender} and it is {self.age} years old.")
    
    def increment_login_attrmpts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0

    def number_login_attempts(self):
        print(f"Login attempts = {self.login_attempts}.")

# class Privileges():
#     def __init__(self):
#         self.privileges = [
#             'allowed for add messages',
#             'allowed for add users',
#             'allowed for delete users',
#             'allowed for ban users'
#             ]

#     def show_privileges(self):
#         for privilege in self.privileges:
#             # print(f"User {self.first_name.title()} {self.last_name.title()}is {privilege}.")
#             print(f"User is {privilege}.")

# class Admin(User):
#     def __init__(self, first_name, last_name, age, gender, login_attempts):
#         super().__init__(first_name, last_name, age, gender, login_attempts)
#         self.privileges = Privileges()

# user = User('oleg', 'bir', 35, 'male', 0)
# user2 = User('liza', 'bir', 26, 'female', 0)
# user3 = User('urmass', '', 7, 'cat', 0)

# user.describe_user()
# user.increment_login_attrmpts()
# user.number_login_attempts()
# user.reset_login_attempts()
# user.number_login_attempts()

# user2.describe_user()
# user2.increment_login_attrmpts()

# user3.describe_user()
# user3.increment_login_attrmpts()