class Privileges():
    def __init__(self):
        self.privileges = [
            'allowed for add messages',
            'allowed for add users',
            'allowed for delete users',
            'allowed for ban users'
            ]

    def show_privileges(self):
        for privilege in self.privileges:
            # print(f"User {self.first_name.title()} {self.last_name.title()}is {privilege}.")
            print(f"User is {privilege}.")