car = 'subaru'
print("Is car == 'subaru'? I predict True.")
print(car == 'subaru')
print("\nIs car == 'audi'? I predict False.")
print(car == 'audi')

str = 'la la la'
song = 'la la la'
if song == str:
    print('\nThis is the same')

list = [10, 20, 30]
if 10 in list:
    print("\n10 is in the list")
if 15 not in list:
    print("\n15 isn't in the list")

if 3 == len(list):
    print("\n list contains three figures")

if len(list) >= 2:
    print("\n list contains more than two figures")

if list[0] == 10 and len(list) > 2:
    print("\n list is ok")