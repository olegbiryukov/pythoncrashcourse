my_food = ["pizza", "falafel", "carrot cake", "potato"]
friends_food = my_food[:]

print("My favorite food are:")
print(my_food)
print("\nMy friend's favorite food are:")
print(friends_food)

my_food.append("vodka")
friends_food.append("ice cream")

print("My favorite food are:")
print(my_food)
print("\nMy friend's favorite food are:")
print(friends_food)

print('The first three items in the list are:')
print(my_food[:3])
print('Three items from the middle of the list are:')
print(my_food[1:3])
print('The last three items in the list are:')
print(my_food[-3:])

for food in my_food:
    print(food.title())

for food in friends_food:
    print(food.title())