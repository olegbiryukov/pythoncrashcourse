cities = {
    'kaliningrad': {
        'country': 'russia',
        'population': 500000,
        'fact': 'it was the Koenigsberg before'
    },
    'sankt-petersburg': {
        'country': 'russia',
        'population': 4000000,
        'fact': 'it was built by Peter The Great'
    },
    'moscow': {
        'country': 'russia',
        'population': 10000000,
        'fact': 'tt is capital of Russia'
    }
}

for city, info in cities.items():
    print(f"\nCity: {city.title()}")
    country = info['country']
    population = info['population']
    fact = info['fact']
    print(f"It placed in {country.title()}, has {population} people and {fact}")