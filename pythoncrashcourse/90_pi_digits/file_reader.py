file_path = '/home/oleg/code/lessons/python crash course/90_pi_digits/pi_digits.txt'

with open(file_path) as file_object:
    contents = file_object.read()
print(contents.rstrip())


with open(file_path) as file_object:
    for line in file_object:
        print(line.rstrip())


with open(file_path) as file_object:
    lines = file_object.readlines()

pi_string = ''
for line in lines:
    pi_string += line.strip()

print(pi_string)
print(len(pi_string))