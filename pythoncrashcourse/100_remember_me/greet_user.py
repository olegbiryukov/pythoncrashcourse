import json

filename = "/home/oleg/code/lessons/pythoncrashcourse/100_remember_me/username.json"

with open(filename) as f:
    username = json.load(f)
    print(f"Welcome back, {username.title()}")