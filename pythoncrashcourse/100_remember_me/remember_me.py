import json

def get_stored_username():
    """Получает хранимое имя пользователя, если оно существует."""
    try:
        with open(filename) as f:
            username = json.load(f)
    except FileNotFoundError:
        return None
    else:
        return username

def get_new_username():
    """Запрашивает новое имя пользователя."""
    username = input("What is your name? ")
    with open(filename, 'w') as f:
        json.dump(username, f)
    return username
    
def greet_user():
    """Приветствует пользователя по имени."""
    username = get_stored_username()
    if username:
        print(f"Welcome back, {username.title()}")
        flag = input(f"{username.title()} is your name? y/n: ")
        if flag != "y":
            username = get_new_username()

    else:
        username = get_new_username()
        print(f"We'll remember you when you come back, {username.title()}!")

filename = '/home/oleg/code/lessons/pythoncrashcourse/100_remember_me/username.json'
greet_user()