mister_x = {
    'first_name': 'unknown',
    'last_name': 'unknown',
    'age': 'around 35',
    'city': 'bobruysk'
}

print(f"Mister X is known as {mister_x['first_name'].title()}!")
print(f"He is {mister_x['age']} years old.")
print(f"He lives in {mister_x['city'].title()} city!")

mister_y = {
    'first_name': 'oleg',
    'last_name': 'balakin',
    'age': '36',
    'city': 'kaliningrad'
}

mister_z = {
    'first_name': 'anton',
    'last_name': 'subbotin',
    'age': '33',
    'city': 'moskow'
}

people = [mister_x, mister_y, mister_z]

for guy in people:
    print(f"Mr.{guy['first_name'].title()} {guy['last_name'].title()} is {guy['age']} years old and lives in {guy['city'].title()}.")