motorcycles = ['honda', 'KTM', 'kawasaki']
print(motorcycles)
motorcycles[1] = 'ducati'
print(motorcycles)

motorcycles.append('yamaha')
print(motorcycles)

motorcycles.insert(0, 'suzuki')
print(motorcycles)

del motorcycles[0]
print(motorcycles)

popped_motorcycle = motorcycles.pop()
print(motorcycles)
print(popped_motorcycle)

first_owned = motorcycles.pop(0)
print(f"The first motorcycle I owned was a {first_owned.title()}.")
print(motorcycles)

motorcycles.remove('ducati')
print(motorcycles)

too_expensice = 'kawasaki'
motorcycles.remove(too_expensice)
print(motorcycles)
print(f"\nA {too_expensice.title()} is too expensive for me.")