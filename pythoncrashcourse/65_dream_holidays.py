responses = {}

polling_active = True

while polling_active:
    name = input("\nWhat is your name? ")
    response = input("Which place do you want to visit on your holidays? ")

    responses[name] = response

    repeat = input("Would you like to let another person respond (y/n) ")
    if repeat == 'n':
        polling_active = False

print(("\n--- Poll Results ---"))
for name, response in responses.items():
    print(f"{name.title()} would like to visit {response.title()}.")