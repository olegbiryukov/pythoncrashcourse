dictionary = {
    'list': 'Список',
    'touple': 'кортеж',
    'dict': 'словарь',
    'set': 'множество',
    'str': 'строка'
}

for word, translate in dictionary.items():
    print(f"\n {word} - это {translate}.")