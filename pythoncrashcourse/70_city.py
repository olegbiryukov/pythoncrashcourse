def describe_city(city, country='russia'):
    print(f"{city.title()} is in {country.title()}")

describe_city('warsaw', 'poland')
describe_city('kaliningrad')
describe_city(country='Philippins', city='Manila')