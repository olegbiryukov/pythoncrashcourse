names = ['oleg', 'ivan', 'roman', 'artyom']

print(names)

print(names[0].title())
print(names[1].title())
print(names[2].title())
print(names[3].title())

print(f'Privet {names[0].title()}, kak dela?')
print(f'Privet {names[1].title()}, kak dela?')
print(f'Privet {names[2].title()}, kak dela?')
print(f'Privet {names[3].title()}, kak dela?')

moto = ['honda', 'kawasaki', 'KTM']

print(f'Ya hotel bi kupit {moto[0].title()}!')
print(f'Ya hotel bi kupit {moto[1].title()}!')
print(f'Ya hotel bi kupit {moto[2]}!')