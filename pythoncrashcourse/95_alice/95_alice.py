def count_words(filename):
    """Подсчет приблизительного количества строк в файле."""
    try:
        with open(filename, encoding='utf-8') as f:
            content = f.read()
    except FileNotFoundError:
        print(f"Sorry, the file {filename} does not exist.")
    else:
        # Подсчет приблизительного количества строк в файле.
        words = content.split()
        num_words = len(words)
        print(f" The file {filename} has about {num_words} words.")

filenames = [
    '/home/oleg/code/lessons/python crash course/95_alice/alice.txt',
    '/home/oleg/code/lessons/python crash course/95_alice/gatsby.txt',
    '/home/oleg/code/lessons/python crash course/95_alice/siddhartha.txt'
]

for filename in filenames:
    count_words(filename)