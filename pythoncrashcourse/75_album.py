def make_album(name_band, name_album, num_tracks=None):
    album = {'name': name_band, 'album': name_album}
    if num_tracks:
        album['tracks'] = num_tracks
    return album

while True:
    x = input("Enter name of band: ")
    y = input("Enter name of album: ")
    z = input("Enter number of tracks: ")
    print(make_album(x, y, z))
    f = input("Finish? (y)")
    if f == 'y':
        break