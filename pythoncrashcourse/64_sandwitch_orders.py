sandwich_orders = ['s maslom', 's kolbasoy', 's syrom', 's maslom', 's chesnokom', 's maslom']
finished_sandwiches = []

print("Maslo zakonchilos'.")
while 's maslom' in sandwich_orders:
    sandwich_orders.remove('s maslom')

while sandwich_orders:
    current_sandwich = sandwich_orders.pop()
    print(f"\nI made your buterbrot {current_sandwich}.")
    finished_sandwiches.append(current_sandwich)

print("\nAll these butery already done:")
for finished_sandwich in finished_sandwiches:
    print(f"{finished_sandwich}")