prompt = ('\nPlease, choose toppings for your pizza?')
prompt += ('\nType "quit" when you finish. ')

topping = ''

while topping != 'quit':
    topping = input(prompt)
    print(f"{topping.title()}'s been added in your order")
