from random import randint

class Die():
    def __init__(self, sides=6):
        self.sides = sides

    def roll_die(self):
        number = randint(1, self.sides)
        print(number)

die = Die()

for i in range(10):
    die.roll_die()

die2 = Die(10)

for i in range(10):
    die2.roll_die()

die3 = Die(20)

for i in range(10):
    die3.roll_die()