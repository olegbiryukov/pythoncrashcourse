#age = int(input("What is your age? "))

flag = True

while flag:
    age = input("What is your age? Type 'quit' for exit: ")
    
    if age == 'quit':
        print('See you again!')
        break
    else:
        age = int(age)

        if age < 3:
            print(F'You are {age} years old so ticket is free for you')
            flag = False
        
        elif age >= 3 and age <= 12:
            print(F'You are {age} years old so ticket costs $10')
            flag = False

        elif age > 12:
            print(F'You are {age} years old so ticket costs $15')
            flag = False