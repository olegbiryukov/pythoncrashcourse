from random import choice

loto = [4, 'f', 8, 3, 'r', 7, 'w', 'h', 'c', 'p']
my_ticket = ['w', 8, 'p', 4]
ticket = []
count = 0

while ticket != my_ticket:
    ticket.clear()
    win = ''
    for i in range(4):
        roll = choice(loto)
        ticket.append(roll)
        win += str(roll)
    count += 1

print(f"Ticket with combination \"{win.upper()}\" is winner! Number of attempts is {count}!")