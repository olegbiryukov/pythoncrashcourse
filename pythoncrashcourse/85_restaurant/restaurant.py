class Restaurant():
    def __init__(self, restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.number_served = 0
    
    def describe_restaurant(self):
        print(f"\"{self.restaurant_name.title()}\" restaurant has {self.cuisine_type} type kitchen.")
    
    def open_restaurant(self):
        print(f"Restaurant \"{self.restaurant_name.title()}\" is opened!")
    
    def work_restaurant(self):
        print(f"{self.number_served} was served!")
    
    def set_number_served(self, number):
        self.number_served = number
    
    def increment_number_served(self, number):
        self.number_served += number
 
class IceCreamStand(Restaurant):
    def __init__(self, restaurant_name, cuisine_type):
        super().__init__(restaurant_name, cuisine_type)
        self.flavors = ['vanilla', 'chokolate', 'sterberry']

    def print_flavors(self):
        for flavor in self.flavors:
            print(f"In {self.restaurant_name.title()} you can buy {flavor.title()}")


# my_restaurant = Restaurant('u olega', 'korean')
# his_restaurant = Restaurant('elena', 'russian')
# her_restaurant = Restaurant('pam pam pam', 'american')

# her_restaurant.describe_restaurant()
# his_restaurant.describe_restaurant()
# my_restaurant.describe_restaurant()
# my_restaurant.open_restaurant()


# restaurant = Restaurant('baltica', 'prussian')
# restaurant.work_restaurant()
# restaurant.number_served = 10
# restaurant.work_restaurant()
# restaurant.set_number_served(20)
# restaurant.work_restaurant()
# restaurant.increment_number_served(5)
# restaurant.work_restaurant()