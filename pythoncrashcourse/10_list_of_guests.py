list_of_guests = ['Lena', 'Valya', 'Nikolay']

for i in list_of_guests:
    print(f"{i}, come to my house on the next weekend!")

print(f"Oh, {list_of_guests[0]} can't come =(")
del list_of_guests[0]
list_of_guests.insert(0, "Liza")

for i in list_of_guests:
    print(f"{i}, come to my house on the next weekend!")

print("Some new friends can join us!")
list_of_guests.insert(0, "Leonid")
list_of_guests.insert(2, "Lyuda")
list_of_guests.append("Kolya")

for i in list_of_guests:
    print(f"{i}, come to my house on the next weekend!")

print("I'm so sorry but I have only 2 places =(")

for i in range(len(list_of_guests)):
    if len(list_of_guests) > 2:
        print(f"Sorry but I dont have place for you, {list_of_guests.pop()} =(")

for i in list_of_guests:
    print(f"{i}, just come!)")

print(f"Numbers of guests is {len(list_of_guests)}")

del list_of_guests[0:2]

print(list_of_guests)