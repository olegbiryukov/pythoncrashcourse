print({i for i in range(1,21)})

numbers = [i for i in range(1,1000001)]
#print(numbers)

print(min(numbers))
print(max(numbers))
print(sum(numbers))

print({i for i in range(1,21,2)})

print([i*i*i for i in range(3,30)])

print([i**3 for i in range(1,10)])

cubes = list(i**3 for i in range(1,10))
print(cubes)