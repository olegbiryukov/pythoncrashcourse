def make_shirt(size='L', text='Тагиил!'):
    print(f"Size is {size} and text should be {text}.")

make_shirt('XL', '\"Можем повторить!\"')
make_shirt(text='\"За дедов!\"', size='M')
make_shirt('XXL')