def make_hamburger(*ingridients):
    for ingridient in ingridients:
        print(f"{ingridient} has been added!")

make_hamburger('ketchup')
make_hamburger('ketchup', 'onion', 'salad')