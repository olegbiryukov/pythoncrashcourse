name = "Eric"
message = f"Hello {name}, would you like to learn some Python today?"
print(message)

print(name.title())
print(name.lower())
print(name.upper())

quote = 'once said, "A person who never made a mistake never tried anything new."'

famous_person = "Albert Einstein"
message = f"\n{famous_person} {quote}"
print(message)

# add spaces
quote = 'once said,\n\t"A person who never made a mistake\n\tnever tried anything new."'

famous_person = "  Albert Einstein "
message = f"\n\t{famous_person.strip()} {quote}" # remove spaces
print(message)