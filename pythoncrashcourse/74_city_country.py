def city_country(city, country):
    message = f"{city.title()}, {country.title()}"
    return message

place = city_country('moscow', 'russia')
print(place)
place = city_country('warsaw', 'poland')
print(place)
place = city_country('kiev', 'ukrain')
print(place)