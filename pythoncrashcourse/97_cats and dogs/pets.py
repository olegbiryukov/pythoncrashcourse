def show_pet_names(filename):
    try:
        with open(filename) as f:
            content = f.read()
    except FileNotFoundError:
        # print(f"Sorry, the file {filename} does not exist.")
        pass
    else:
        print(content)

files = [
    '/home/oleg/code/lessons/python crash course/97_cats and dogs/cats.txt',
    '/home/oleg/code/lessons/python crash course/97_cats and dogs/dogs.txt',
    '/home/oleg/code/lessons/python crash course/97_cats and dogs/lizzards.txt'
]

for file in files:
    show_pet_names(file)