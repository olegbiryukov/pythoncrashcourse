filename = '/home/oleg/code/lessons/python crash course/93_guest/guest_book.txt'

while True:
    print('Type "q" for quit.')
    name = input('Enter your name: ')
    if name == 'q':
        break
    else:
        print(f'Hello, {name.title()}!')

        with open(filename, 'a') as file_object:
            file_object.write(f"{name.title()}\n")