pet_1 = {
    'name': 'bossya',
    'spicies': 'cat',
    'owner': 'oleg'
}

pet_2 = {
    'name': 'urmass',
    'spicies': 'cat',
    'owner': 'liza'
}

pet_3 = {
    'name': 'janya',
    'spicies': 'dog',
    'owner': 'lyuda'
}

pets = [pet_1, pet_2, pet_3]

for pet in pets:
    print(f"{pet['owner'].title()} has a {pet['spicies']} with a name {pet['name'].title()}")